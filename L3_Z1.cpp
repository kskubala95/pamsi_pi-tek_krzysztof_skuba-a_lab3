#include <iostream>
#include <string>
#include <ctime>
#include <windows.h>

using namespace std;
//template <typename TypEl>
class Stos
{
	private:
		long int rozmiar;
		//TypEl *T;
		int *T;
		int top=-1;
		
	
	public:
		string EmptyStackException="Stos jest pusty";
		string FullStackException="Stos jest pelny";
	
	
	//  Metody
		TabStack(int r)
		{
			rozmiar=r;
			//T=new TypEl[rozmiar];
			T=new int[rozmiar];
		}

		bool isEmpty()
		{
			return (top<0);
		}
		
		int usunjeden()		//TypeEl usunjeden()
		{
			if (isEmpty())
			{
					//throw EmptyStackException;// ("Stos pusty");
			}
			else
			{
			//	T[top]=NULL;     // czy to w ogole dziala ? Chcialem usunac niby ten element przed uprzednim zmniejszeniem tablicy 
				--rozmiar;
			}
				return T[rozmiar];	
		}
		
		int usunwszystko()//TypEl usunwszystko()
		{
			if (isEmpty())
			{
					//throw EmptyStackException;// ("Stos pusty");
			}
			else
			{
				for (int i=top;i>=0;i--)
				{
			//	T[i]=NULL;
				--rozmiar;
				}
			}
				return T[rozmiar];	
		}

		void wysw()
		{
			cout<< "Zawartosc stosu: "<<endl;
			cout<<endl;
			for (int i=0;i<rozmiar;i++)
			{
				cout<< T[i] << endl;
			}
		}
		

		void push(int o)   //TypEl o
		{
			++top;
			if (top>=rozmiar-1)
			{
			//	throw FullStackException;			
				++rozmiar;
				T[top]=o;
			}
			else
			T[top]=o;
		}
		
		
		void pseudo(long int n)
		{
			int x;
			
			for (long int i=0;i<n;i++)
			{
			x =(rand() % 101);
			push(x);
			}
		}
	
	
	};

void menu()
{
	
	cout<< "1.  Dodaj na stos."<<endl;
	cout<< "2.  Zdejmij ze stosu jeden element."<<endl;
	cout<< "3.  Wyswietl zawartosc stosu ."<<endl;
	cout<< "4.  Zdejmij ze stosu wszystko."<<endl;
	cout<< "5.  Wypelnij liczami losowymi (z pomiarem czasu)."<<endl;
	cout<< "0.  Koniec programu."<<endl;
}


int main()
{
	LARGE_INTEGER frequency;
    LARGE_INTEGER t1, t2; 
    long double elapsedTime;
	int liczba,liczba1;
	int wybor;
	int rozmiars=0;
	Stos obiekt;
	//Stos <long int> obiekt;
	
	obiekt.TabStack(rozmiars);
	QueryPerformanceFrequency(&frequency);
	
	menu();
	cout<< "Co chcesz zrobic ?"<<endl;
	cin>> wybor;

	while(wybor!=0)
	{
	switch(wybor)
	{
		case 1: 
		cout<< "Prosze podac wartosc jaka chcesz umiescic na stosie: ";
		cin>>liczba;
		obiekt.push(liczba);
		break;
		
		case 2:
		obiekt.usunjeden();
		break;
		
		case 3:
		obiekt.wysw();
		break;
		
		case 4:
		obiekt.usunwszystko();
		break;
			
		case 5:
		cout<< "Prosze podac ile liczb chcesz umiescic na stosie: ";
		cin>>liczba1;
		QueryPerformanceCounter(&t1);
		obiekt.pseudo(liczba1);
		QueryPerformanceCounter(&t2);
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		cout<<"Czas operacji: "<< elapsedTime<< " ms.\n" <<endl;
		break;
	}
	cout<< "Co chcesz zrobic ?"<<endl;
	menu();
	cin>> wybor;
	} 
	
	return 0;
	}
