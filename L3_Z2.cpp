#include <iostream>
#include <string>
#include <ctime>
#include <windows.h>
using namespace std;
template <typename TypEl>
class Kolejka
{
	private:
		long int licznik=0,rozmiar;
		int g;
		TypEl *T;
		int top=-1;
		
	
	public:
		string EmptyStackException="Stos jest pusty";
		string FullStackException="Stos jest pelny";
	
	
	//  Metody
		TabStack(int r)
		{
			rozmiar=r;
			T=new TypEl[rozmiar];
		}

		bool isEmpty()
		{
			return (top<0);
		}
		
		TypEl usunjeden()
		{
			if (isEmpty())
			{
					//throw EmptyStackException;// ("Stos pusty");
			}
			else
			{
			//	T[top]=NULL;     // czy to w ogole dziala ? Chcialem usunac niby ten element przed uprzednim zmniejszeniem tablicy 
				--rozmiar;
			}
				return T[rozmiar];	
		}
		
		TypEl usunwszystko()
		{
			if (isEmpty())
			{
					//throw EmptyStackException;// ("Stos pusty");
			}
			else
			{
				
				for (int i=top;i>=0;i--)
				{
				--rozmiar;
				
				}
			}
				return T[rozmiar];	
		}

		void push(TypEl o)
		{
			++top;
			++licznik;
			if (top>rozmiar-1)
			{
			//	throw FullStackException;
				++rozmiar;
				for (int i=licznik;i>1;i--){
				T[i-1]=T[i-2];
			}
			T[0]=o;
			}
			else
			{
			T[0]=o;
		//	g=T[top];
			}
		}
		
		void wysw()
		{
			cout<< "Zawartosc stosu: "<<endl;
			cout<<endl;
			for (int i=0;i<rozmiar;i++)
			{
				cout<< T[i] << endl;
			}
		}
		
		void pseudo(long int n)
		{
			int x;
			
			for (long int i=0;i<n;i++)
			{
			x =(rand() % 101);
			push(x);
			}
		}
	};

void menu()
{
	
	cout<< "1.  Dodaj do kolejki."<<endl;
	cout<< "2.  Usun z kolejki jeden element."<<endl;
	cout<< "3.  Wyswietl zawartosc kolejki ."<<endl;
	cout<< "4.  Usun z kolejki wszystko."<<endl;
	cout<< "5.  Wypelnij liczami losowymi (z pomiarem czasu)."<<endl;
	cout<< "0.  Koniec programu."<<endl;
}


int main()
{
	LARGE_INTEGER frequency;
    LARGE_INTEGER t1, t2; 
    long double elapsedTime;
	int liczba,liczba1;
	int wybor;
	int rozmiars=1;
	Kolejka <long int> obiekt;
	
	obiekt.TabStack(rozmiars);
	
	menu();
	cout<< "Co chcesz zrobic ?"<<endl;
	cin>> wybor;

	while(wybor!=0)
	{
	switch(wybor)
	{
		case 1: 
		cout<< "Prosze podac wartosc jaka chcesz umiescic w kolejce: ";
		cin>>liczba;
		obiekt.push(liczba);
		break;
		
		case 2:
		obiekt.usunjeden();
		break;
		
		case 3:
		obiekt.wysw();
		break;
		
		case 4:
		obiekt.usunwszystko();
		break;
		
		case 5:
		cout<< "Prosze podac ile liczb chcesz umiescic na stosie: ";
		cin>>liczba1;
		QueryPerformanceCounter(&t1);
		obiekt.pseudo(liczba1);
		QueryPerformanceCounter(&t2);
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		cout<<"Czas operacji: "<< elapsedTime<< " ms.\n" <<endl;
		break;
				
	}
	cout<< "Co chcesz zrobic ?"<<endl;
	menu();
	cin>> wybor;
	} 
	
	return 0;
	}
